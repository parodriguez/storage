:open_file_folder:
SERVIDOR NFS
============

:paperclip:
Instalación y Configuración de NFSv4
--------------------


#### *1. Instalación*
* Para instalar el `NFSv4` como servidor, ejecutamos lo siguiente:
```
sudo apt-get install nfs-kernel-server 
```
* Levantamos el servicio con los comandos:
```
 systemctl enable nfs-kernel-server
 systemctl start nfs-kernel-server
```

En el caso de la instalación de los clientes varió un poco en los diferentes paquetes.
* Para **Web** que ocupó Debian 9 se pidió que instalaran:

```
sudo apt-get install nfs-acl-tools
```
* Para el caso de **Mail** que ocupó CentOS 6 se pidió que instalaran:
```
sudo yum install nfs-utils nfs-utils-libs
```
<!-- blank line -->
----
<!-- blank line -->
#### *2.Configuración*
* Para exportar los directorios creamos los directorios `/srv/home/` para compartir con el equipo de **Mail** y `/srv/www` para compartir
con el equipo de **Web**.
Ambas carpetas fueron agregadas con los permisos 775.
Se verificó la existencia del usuario *www-data* que se crea a partir de la instalación y configuración de Apache2 y se encontró su UID y su GID ambos 33.
Esto únicamente para el directorio que será compartido con **Web**

* Se configuró el archivo `/etc/exports` en el cual añadimos las siguientes lineas:

```
/srv/home mail.becarios.tonejito.info(rm,sync,no_root_squash)   
/srv/www web.becarios.tonejito.info(rw,sync,all_squash,anonuid=33,anongid=33)
```

y se reinició el servicio.

* Asesoramos a ambos equipos para iniciar el servicio de nfs y montar la carpeta que les estábamos compartiendo con el comando
```
mount -t nfs4 -o rw,nosuid storage.becarios.tonejito.info:/srv/directorio_compartido /srv/directorio_correspondiente
```
y la configuración del archivo `/etc/fstab` para que se montara el directorio cada vez que se inicie el equipo.

:paperclip:
Usuarios NFS
--------------------

#### *Configuración de la conexión con LDAP*
* Para realizar la configuración con LDAP, se debe instalar LDAP para eso, instalamos el siguiente paquete:   
```
sudo apt install  libnss-ldapd libpam-ldapd nscd
```

Al realizar la instalación se abrirán algunas pantallas para realizar configuración: 

1.	En la primera colocaremos la URI de nuestro servidor LDAP que es: *ldaps://directory.becarios.tonejito.info:636*      
2.	En la segunda pantalla colocarelos la Base del servidor LDAP que es: *dc=directory,dc=becarios,dc=tonejito,dc=info*      
3.	La tercera pantalla nos indicará que se realizarán configuraciones de  `libnss-ldapd` por lo cual daremos en Ok   
4.	La ultima pantalla se seleccionaran las configuraciones para `libnss-ldapd`   
    
Se finaliza la instalación.   

* Al terminar la instalación, vamos a revisar algunos archivos:   
 1. El primer archivo a configurar es `ldap.conf`, lo podemos abrir con el editor de preferencia.   
```
sudo vi /etc/lapd/ldap.conf
```
De acuerdo a lo indicado por nuestro servidor LDAP, el archivo en su contenido queda de la siguiente manera:

```
BASE    dc=directory,dc=becarios,dc=tonejito,dc=info   
URI     ldaps://directory.becarios.tonejito.info:636 
 
#SIZELIMIT       12   
#TIMELIMIT       15   
#DEREF           never   
 
#TLS certificates (needed for GnuTLS)   
TLS_CACERTDIR     /etc/ssl/certs   
TLS_CACERT           /etc/ssl/certs/0001_chain.pem   
TLS_REQCERT        demand   
```

2. El siguiente archivo a configurar, es `nsswitch.conf` este archivo, se configura las prioridades, en las que se buscarán   
   tanto a los usuarios, como grupos, por lo cual, se debe agregar LDAP     

Editamos el archivo para poder realizar las modificaciones:
```
sudo vi /etc/nsswitch.conf 
```

El archivo debe quedar de la siguiente manera:

```
passwd:         compat ldap   
group:          compat ldap   
shadow:         compat ldap   
gshadow:        files   
 
hosts:          files dns ldap   
networks:       files ldap   
 
protocols:      db files ldap   
services:       db files ldap   
ethers:         db files ldap   
rpc:            db files ldap   
 
netgroup:     ldap   
aliases:      ldap   
```

3. El siguiente archivo a configurar es `common-session`, este archivo se configura, ya que si al iniciar sesión con un usuario   
    este aun no tiene creada si carpeta en `/srv/home`, esta se creerá.  

Por lo cual, editamos el archivo:   
```
sudo vi /etc/pam.d/common-session
```
Y al final del archivo agregamos:   
>session required  pam_mkhomedir.so

4. El último archivo, es `common-auth`, en este archivo, en la línea que comienza con  `auth requisite` agregamos `nullok_secure`,   
   no hay mucha información sobre esto, pero esta línea se agrega para poder  soportar pam_unix sin contraseña de inicio de sesión solo otra tty.   

Esto, se edita en el archivo:
```
sudo vi /etc/pam.d/common-auth
```

La línea anteriormente mencionada, queda de la siguiente manera:   
>auth required                       pam_permit.so nullok_secure

* Despues de realizar todas las modificaciones en los archivos mencionados, se requiere reiniciar el servidor ncsd, para esto ejecutamos
```
/etc/init.d/nscd restart
```
* Para comprobar que se tiene acceso al servidor LDAP, se hace uso de ldapsearch, para esto tenemos que instalar el siguiente paquete:
```
sudo apt-get isntall ldap-utils
```
Se ejecutan los comandos:
```
ldapsearch –v –x –H ´ldaps://directory.becarios.tonejito.info:636´ –b ´dc=directory,dc=becarios,dc=tonejito,dc=info´   
ldapsearch –v –x –H ´ldaps://directory.becarios.tonejito.info:636´ –W -D ´cn=admin,dc=directory,dc=becarios,dc=tonejito,dc=info´
```
* El primer comando, nos muestra las configuraciones para los usuarios que LDAP creo:

```
#Patricia Gomez, user, directory.becarios.tonejito.info
dn: cn=Patricia Gomez, ou=user, dc=directory, dc=becarios, dc=tonejito, dc=info   
objectClass: posxAccount   
objectClass: inetOrgPerson   
objectClass: organizationalPerson   
objectClass: person   
homeDirectory: /srv/home/pgomez   
uid: pgomez   
cn: Patricia Gomez   
uidNumber: 10032   
gidNumber: 10000   
sn: Gomez   
givenName: Patricia   
loginShell: /bin/false   
mail: patricia.gomez@becarios.tonejito.info   
```

* El segundo comando, nos muestra algo similar, pero antes, nos pide una contraseña, la cual es proporcionado por el servidor LDAP,   
  además agrega el hash de la contraseña del usuario:

```
#Diana Tadeo, user, directory.becarios.tonejito.info
dn: cn=Diana Tadeo, ou=user, dc=directory, dc=becarios, dc=tonejito, dc=info      
objectClass: posxAccount   
objectClass: inetOrgPerson   
objectClass: organizationalPerson   
objectClass: person   
homeDirectory: /srv/home/dtadeo   
uid: dtadeo   
cn: Diana Tadeo   
`userPassword:e1NIQX1DTnc10WRMdW43RzB4MH1XeTRTSkpGY1NkYmc9`   
uidNumber: 10030   
gidNumber: 10000      
sn: Tadeo   
givenName: Diana      
loginShell: /bin/false   
mail: diana.tadeo@becarrios.tonejito.info
```

* En caso de no poder ejecutar los comandos, revisar tanto el el archivo `ldap.conf` como en la linea de comandos, que se este ingresando   
  correctamente la **BASE** y la **URI** de nuestro servidor **LDAP*.Del mismo modo que si se utilizará una opción de `ldapsearch` que   
  pida contraseña como, en el segundo comando que se pide ingresar, se agrege el `cn=admin`.

* Ahora, necesitamos obtener el contenido de los archivos passwd y groups, de nuestro servidor LDAP, para esto ejecutamos los siguientes comandos

```
getent passwd
getent group
```
Con esto, no se quiere decir que como tal, cada que ingrese a su archivo /etc/passwd, ya a obtener los datos del `passwd` del servidor **LDAP**   
mas bien, cada que se ejecute el comando `getent` se va a listar además del contenido de nuestro `passwd` el de nuestro servidor **LDAP**. Lo mismo   
con el archivo group.

Del mismo modo, si hacemos uso del comando `id usuario`en donde usuario es el `uid` del usuario, vamos a obtener la información conrrespondiente   
a ese usuario, para el usuario **ileal**:

```
id ileal
uid=10031(ileal) gid=10000(becarios) groups=10000(becarios) 
```

* Si, al ejecutar el comando `getent passwd` no ve nada mas que el `passwd` propio, se debe de revisar la configuración de los primeros cuatro   
  archivos presentados al inicio de este apartado, en especial el archivo `ldap.conf` y `nsswitch.conf` y verificar que se reinicia el servidor    
  nscd con el comando `/etc/init.d/nscd restart`

* Por último, cada uno de los usuarios que LDAP cree debe tener su correspondiente carpeta en `/srv/home/`, para esto, se hará uso de la modificación   
  realizada en el archivo `common-session` en donde indicamos que cuando el usuario iniciará sesión por primera vez, se creará su carpeta   
  correspondiente de home, por lo cual, se iniciará sesión con cada uno de los usuarios, así creamos las carpetas en donde además si revisamos   
  `/srv/home/` con un `ls -la`nos daremos cuenta que cada carpeta corresponde a su usuario, pues es el dueño, y todos pertenecen al grupo que    
  indico **LDAP**, en este caso, **becarios**. 
 
  Para iniciar sesión con los usuarios ingresamos:

```
sudo login
```
Al dar enter despues de ingresar el comando anterior, nos aparecerá lo siguiente:

```
storage.becarios.tonejito.info login: 
```
Se ingresa el usuario con el que se quiere iniciar sesión, posteriormente nos pedirá la contraseña, se ingresará dicha contraseña y nos dará el login   
como es la primera vez que se inicia sesión al final nos mostrará un mensaje de que se acaba de crear la carpeta `/srv/home/usuario`

Como nota, si se quiere volver a iniciar sesión se puede realizar, ya no aparecerá el mensaje de la creación de la carpeta, puesto que ya se hizo,   
pero, no nos dará ningún bash, por configuraciones de **LDAP** 

Del mismo modo, si se quiere iniciar sesión con un usuario, que **LDAP** no creo, el acceso será negado, pues no existe dicho usuario.   

Del usuario **dmartin**:   
```
sudo login
storage.becarios.tonejito.info login: dmartin
Password: 
```
Revisando la carpeta `/srv/home` con `ls -la`, revisamos las caracteristicas de la carpeta de dicho usuario, confirmando que ya esta creada   

```
cd /srv/home/
ls -la

drwxr-xr-x  2 dmartin   becarios   4096   Oct 19   17:27  dmartin

```

#### *Configurar al usuario www-data como propietario del directorio /srv/www*

* Como se menciono antes, se reviso que tanto **Web** como **Storage** tuvieran al usuario **www-data** con el mismo `uid` y `gid`, por lo cual   
  lo unico que se realizó fue colocar como propierario del directorio a dicho usuario, ejecutando:

```
sudo chown -R www-data:www-data /srv/www
```

:paperclip:
Instalación y configuración de la replica de MySQL v5.7
--------------------
*1. Instalación*
Se instaló el servidor de replica con 
```
#sudo apt-get install mysql-server mysql-client
```

*2.Configuración*
Se configuró la replicacion con la dirección IP del equipo database 172.31.45.187 ya que la VPN no fue implementada por el equipo encargado,
los comandos que se utilizaron fueron:
>CREATE DATABASE wordpress;

>EXIT;

Se importo el archivo wordpress.sql del MASTER del equipo "database" y se ejecutaron los siguientes comandos:

```
#mysql -u root -p wordpress < /path/to/wordpress.sql

#sudo nano /etc/mysql/my.cnf
```

Se añadieron las siguientes lineas al archivo my.cnf
```
[mysqld]

server-id=2
```
```
#sudo service mysql restart
```

Se configuro mysql con los datos que nos proporciono el equipo "database" de la siguiente manera:

```
mysql> CHANGE MASTER TO

    ->     MASTER_HOST=172.31.45.187',
    
    ->     MASTER_USER='slave-storage',
    
    ->     MASTER_PASSWORD='hola123,',
    
    ->     MASTER_LOG_FILE='mysql-bin.000002',
    
    ->     MASTER_LOG_POS=154;
```

Se activo el esclavo:

```
>START SLAVE;

Se checó su estado con:

>show slave status;

>show processlist;
```

:paperclip:
Instalación y configuación BackupPC
--------------------
#### *1. Instalación*
* Para instalar `Backuppc` ejecutamos el siguiente comando:
```
$sudo apt install backuppc 
```
Durante la configuración se nos preguntará que tipo de servidor de correo necesitamos por lo que seleccionamos:

>Internet with smarthost

Seguido se nos preguntará si queremos que se reconfigure automaticamente apache2 por lo que damos aceptar.

Ya instalado procedemos a cambiarle la contraseña a backuppc

```
$sudo htpasswd /etc/backuppc/htpasswd backuppc
```

Donde nos pedirá que ingresemos una contraseña, y que confirmemos la contraseña.

#### *2.Configuración*
Nos cambiamos al usuario backuppc
```
$sudo su - backuppc
```

Ahora procedemos a crear una llave privada y una pública con el siguiente comando:

```
$ssh-keygen -t rsa
```

Nos preguntara con que nombre y en que ruta se van a guardar nuestras llaves.

En las máquinas de los clientes en este caso directory y database, se crearon dos usuarios llamado backuppc.

Ahora procedemos a pasar la llave pública a directory y a database para que accedamos por medio de ssh a los clientes por medio del usuario backuppc, una vez que 
copien nuestra llave pública en los usuarios backuppc en directorio .ssh dentro del archivo authorized_keys. Procedemos a entrar por medio de ssh a database y a 
directory.

```
$ssh -i llaveprivadabackuppc backuppc@database.becarios.tonejito.info
```

```
$ssh -i llaveprivadabackuppc backuppc@directory.becarios.tonejito.info
```

Backuppc nos permite configurarlo por medio de línea de comandos o por medio de una interfaz gráfica en este caso se configurará por medio de interfaz gráfica, 
para poder ingresar a la interfaz gráfica abrimos un navegador y buscamos la siguiente dirección.

```
>storage.becarios.tonejito.info/backuppc
```

Ya en la interfaz gráfica seleccionamos

```
>Edit Hosts

>Add
```

En nombre del host ponemos: 

```
>database.becarios.tonejito.info
```

La opción de dhcp no la seleccionamos.

En user:

```
>backuppc
```

Volvemos a agregar otro host, la opción de dhcp no la seleccionamos.

```
>directory..becarios.tonejito.info

>backuppc

```
Guardamos, seleccionamos Xfer y en la opción XferMethod seleccionamos rsync, dejamos que haga los respaldos de manera predeterminada.
Ahora, en la parte de la izquierda donde dice Select a host... seleccionamos

```
>database.becarios.tonejito.info
```

Seleccionamos Edit Config, Xfer y ahora en donde dice RsyncShareName escribimos

```
>/usr/bin/
```

Ahora en donde dice RsynClientCmd ponemos lo siguiente

```
>$sshPath -i /home/ignacio-leal/backup_rsa -l backuppc $host $rsyncPath --server --sender --numeric-ids --perms --owner --group -D --links --hard-links --times --block-size=2048 --recursive --ignore-times . /usr/bin/mysqldump /usr/bin/pg_dump
```

Así de esta manera solo respaldaremos los archivos mysqldump y pg_dump, teniendo de esta manera los respaldos de MySQL y de PostgreSQL.

Ahora seleccionamos el host de directory

```
>directory.becarios.tonejito.info
```

Seleccionamos Edit Config, Xfer y ahora en donde dice RsyncShareName escribimos

```
>/etc/ldap/slapd.d/
```

Ahora en donde dice RsynClientCmd ponemos lo siguiente

```
>$sshPath -i /home/ignacio-leal/backup_rsa -l backuppc $host sudo $rsyncPath --server --sender --numeric-ids --perms --owner --group -D --links --hard-links --times --block-size=2048 --recursive --ignore-times . /etc/ldap/slapd.d/cn=config.ldif
```

De esta manera solo respaldaremos el archivo cn=conf.ldif.

Los respaldos se guardan en las siguientes rutas

```
>/var/lib/backuppc/pc/database.becarios.tonejito.info

>/var/lib/backuppc/pc/directory.becarios.tonejito.info/1
```

:paperclip:
Configuración de Let's Encrypt
--------------------
Para la obtención del certificado de Let´s Encrypt, primero ejecutamos el siguiente comando
```
#openssl genrsa -out server.key 2048
```

Ahora procedemos a obtener la solicitud de firma para let's encrypt, nos pedira que llenemos algunos, los llenamos, en este caso el common name es

```
#openssl req -new -days 365 -key server.key -out server.csr
```

>storage.becarios.tonejito.info

Ya que tenemos la petición de firma, ahora clonamos el repositorio de certboot de github, y nos movemos a certbot

```
#git clone https://github.com/certbot/certbot.git
#cd certbot
```

Ejecutamos el siguiente comando

```
#./certbot-auto certonly --authenticator manual --server https://acme-v01.api.letsencrypt.org/directory --text --email ignacio.leal@bec.seguridad.unam.mx --csr server.csr
```

Nos pedira que aceptemos los terminos y condiciones de igual manera nos pedira crear una carpeta y un archivo con un contenido que nos saldrá en pantalla los creamos y le damos siguiente
y se nos crearan tres archivos, ya teniendo estos archivos los guardamos junto en 

```
>/etc/ssl/certs/
```

Y nuestra llave privada la guardamos en

```
>/etc/ssl/private/storage.key
```

Procedemos a modificar el siguiente archivo para que se pueda ingresar por medio de https

```
>/etc/apache2/sites-available/default-ssl.conf
```

Modificamos lo siguiente

```
>ServerName storage.becarios.tonejito.info
```

Agregamos lo siguiente

```
>SSLEngine on

>SSLCertificateFile /etc/ssl/certs/0000_cert.pem

>SSLCertificateKeyFile /etc/ssl/certs/storage.key

>SSLCertificateChainFile /etc/ssl/certs/0000_chai.pem

>SSLCertificateChainFile /etc/ssl/certs/0001_cert.pem
```

Se modifico el archivo donde esta el virtual host que escucha en el puerto 80 para que todas las consultas que se hagan a storage.becarios.tonejito.info se redirijan al puerto 443 de https, 
agregandole la siguiente línea

```
>Redirect permanent / https://storage.becarios.tonejito.info
```

:paperclip:
Instalación y Configuración de Postfix
--------------------

#### *Instalación*

Para instalarlo escribimos
```
sudo apt-get install postfix
```
a continuación lo configuramos dejando el archivo `/etc/postfix/main.cf` de la siguiente forma

```
# See /usr/share/postfix/main.cf.dist for a commented, more complete version


# Debian specific:  Specifying a file name will cause the first
# line of that file to be used as the name.  The Debian default
# is /etc/mailname.
#myorigin = /etc/mailname

smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no

# appending .domain is the MUA's job.
#append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

readme_directory = no

# See http://www.postfix.org/COMPATIBILITY_README.html -- default to 2 on
# fresh installs.
compatibility_level = 2

# TLS parameters
#smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
#smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
#smtpd_use_tls=yes
#smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
#smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.

smtpd_relay_restrictions = permit_mynetworks cated defer_unauth_destination
myhostname = storage.becarios.tonejito.info
mydomain = becarios.tonejito.info
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
#myorigin = /etc/mailname
#mydestination = storage.becarios.tonejito.info, storage.becarios.tonejito.info, becarios.tonejito.info, localhost.becarios.tonejito.info, localhost
mydestination =
relayhost = mail.becarios.tonejito.info
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = ipv4

```

#### *Verificando*

Ahora descargamos mailx con

```
sudo apt-get install bsd-mailx
```
para permitir el envío de correos.

Una vez que los equipos de **Mail** y **Direcotry** han configurado adecuadamente su parte, probamos el envío de algún mensaje con el siguiente comando.
```
sudo echo "Prueba de correo enviado" | mail -- nombre_usuarioldap@becarios.tonejito.info
```
y podemos verificar que se haya recibido el mensaje dentro del directorio del usuario al que fue enviado (incluyendo al nuestro) dentro del directorio `/srv/home/nombre_usuarioldap/Maildir/new`

En caso de que todo esté configurado correctamente se nos mostrarán los mensajes recibidos.

:paperclip:
Configuración de IPTables
--------------------

Para modificar las iptables optamos por una política permisiva.
Instalamos `iptables-persistent` para poder mantener la persistencia de nuestras reglas y después creamos el archivo que va a cargarse para la creación de las reglas

```
*filter
:INPUT ACCEPT [182:31430]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [335:157504]
-A INPUT -p tcp -m tcp --dport 53 -j ACCEPT
-A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
-A INPUT -p udp -m udp --dport 53 -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p tcp -m tcp --dport 25 -j ACCEPT
-A INPUT -s becarios.tonejito.info/32 -j ACCEPT
-A INPUT -s mail.becarios.tonejito.info/32 -j ACCEPT
-A INPUT -s directory.becarios.tonejito.info/32 -j ACCEPT
-A INPUT -s database.becarios.tonejito.info/32 -j ACCEPT
-A INPUT -s web.becarios.tonejito.info/32 -j ACCEPT
-A INPUT -p tcp -m tcp --dport 587 -j ACCEPT
-A OUTPUT -p tcp -m tcp --dport 53 -j ACCEPT
-A OUTPUT -p tcp -m tcp --dport 22 -j ACCEPT
-A OUTPUT -p udp -m udp --dport 53 -j ACCEPT
-A OUTPUT -p icmp -j ACCEPT
-A OUTPUT -p tcp -m tcp --dport 25 -j ACCEPT
-A OUTPUT -p tcp -m tcp --dport 857 -j ACCEPT
-A OUTPUT -d becarios.tonejito.info/32 -j ACCEPT
-A OUTPUT -d mail.becarios.tonejito.info/32 -j ACCEPT
-A OUTPUT -d directory.becarios.tonejito.info/32 -j ACCEPT
-A OUTPUT -d database.becarios.tonejito.info/32 -j ACCEPT
-A OUTPUT -d web.becarios.tonejito.info/32 -j ACCEPT
-A OUTPUT -p tcp -m tcp --dport 587 -j ACCEPT
COMMIT
```
Esto lo hacemos para mantener abiertas los puertos que se estarán ocupando para proporcionar y recibir los servicios respectivos.

Ejecutamos el comando
```
service netfilter-persistent save archivo_de_reglas
```
Reiniciamos el equipo y probamos con `sudp iptables -L` y verificamos que se encuentren las reglas que pusimos.

:paperclip:
Configuración de OpenVPN
--------------------
Se instaló Open VPN con el siguiente comando:
```
#sudo apt install openvpn 
```
Se configuró opnevpn cliente con:
```
#sudo cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/
```
Se genero una peticion de firma y una llave privada con 
```
#openssl req -new -newkey rsa:2048 -nodes -keyout storage.key -out storage.csr
```
Se le entrego al equipo encargado de la CA y fue firmado, el certificado firmado y la llave fueron añadidos a 

 /etc/openvpn/
 
 Se modificó el archivo /etc/openvpn/client.conf para que apuntara a las direcciones de los archivos .key y .crt
 
 Hasta aqui se pudo llegar con ese metodo ya que el equipo encargado de la VPN no proporciono ese servidor.

 Se implemento el archivo storage.opvpn y se hizo la persistencia editando:
```
#sudo nano /etc/default/openvpn
```
 se descomenta la linea
``` 
#AUTOSTAR=ALL
``` 
Y se ejecutan los siguientes comandos:
```
#mv servicio.ovpn servicio.conf
```
```
#mv servicio.conf /etc/openvpn
```
y se reinicia el servicio
```
#sudo systemctl daemon-reload
```

:earth_americas:
Referencias
-----------

#### **NFS**
https://help.ubuntu.com/community/NFSv4Howto   
https://docs.fedoraproject.org/en-US/Fedora/14/html/Storage_Administration_Guide/s1-nfs-client-config-options.html   
https://www.tecmint.com/how-to-setup-nfs-server-in-linux/   
https://www.flecharoja.com/blog/2018-06/exportar-directorio-nfs-cliente-rhel7/   

#### **LDAP**
http://www.juanluramirez.com/configurar-sistema-cuentas-centralizadas-ldap-nfs/   
https://sergiomarchenaquiros.wordpress.com/2014/12/17/configurar-un-sistema-de-cuentas-centralizadas-con-ldap-y-nfs/      
https://enekoamieva.com/tutorial-5-openldap-configurar-un-cliente-en-el-dominio-ldap/   
http://informatica.iescuravalera.es/iflica/gtfinal/libro/x4915.html   
https://enekoamieva.com/servidor-nis-en-linux-configuracion-y-funcionamiento/   
https://www.redhat.com/archives/pam-list/2005-September/msg00001.html   
http://blog.adamsbros.org/2015/06/09/openldap-ssha-salted-hashes-by-hand/   

#### **MySQL**
https://www.digitalocean.com/community/tutorials/how-to-set-up-master-slave-replication-in-mysql   
https://dev.mysql.com/doc/refman/5.7/en/replication-howto.html   
https://dev.mysql.com/doc/refman/5.7/en/replication-setup-slaves.html   
https://dev.mysql.com/doc/refman/5.7/en/replication-snapshot-method.html   

#### **BackupPC**
https://help.ubuntu.com/community/BackupPC   
http://wiki.networksecuritytoolkit.org/index.php/HowTo_BackupPC_SSH_Key_Authentication_Setup_For_rsync_Transfer   
https://wiki.centos.org/es/HowTos/BackupPC   
http://backuppc.sourceforge.net/faq/BackupPC.html#General-server-configuration   
http://backuppc.sourceforge.net/faq/BackupPC.html#Step-5:-Client-Setup   
http://monklinux.blogspot.com/2012/03/backuppc-host-configuration-backing-up.html   
https://www.systemajik.com/setting-up-backuppc-on-ubuntu/   
https://wiki.zmanda.com/index.php/Exclude_and_include_lists   
http://backuppc.sourceforge.net/faq/ssh.html   
https://www.digitalocean.com/community/tutorials/how-to-use-backuppc-to-create-a-backup-server-on-an-ubuntu-12-04-vps   

#### **Let´s Encrypt**
https://github.com/VeliovGroup/ostrio/blob/master/tutorials/ssl/ssl-letsencrypt.md   
https://p5r.uk/blog/2015/using-letsencrypt-in-manual-mode.html   
https://certbot.eff.org/docs/using.html#standalone   

#### **Postfix**
https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-on-ubuntu-16-04   

#### **IPTables**
https://www.digitalocean.com/community/tutorials/how-to-list-and-delete-iptables-firewall-rules   
http://manpages.ubuntu.com/manpages/xenial/man8/netfilter-persistent.8.html   

#### **OpenVPN**
https://www.liquidweb.com/kb/generating-certificate-signing-request-csr-ubuntu-16-04/   
https://www.simplified.guide/ssh/copy-file   
https://help.ubuntu.com/lts/serverguide/openvpn.html.en   

#### **Iniciar Servicios automaticamente después de un reinicio**
https://help.ubuntu.com/community/UbuntuBootupHowto   
https://www.digitalocean.com/community/tutorials/how-to-configure-a-linux-service-to-start-automatically-after-a-crash-or-reboot-part-1-practical-examples


:raised_hands:
Integrantes:
------------
David Martin Jimenez   
Patricia Nallely Gómez Flores   
Diana Tadeo Guillén   
Ignacio Leal González   
